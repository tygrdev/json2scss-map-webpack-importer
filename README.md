**This project has been [moved here](https://gitlab.com/grinn.amy/json2scss-map-webpack-importer).**

# json2scss-map-webpack-importer

Import json files into sass using
[json2scss-map](https://github.com/AS-Devs/json2scss-map)

## Usage

```js
const jsonImporter = require('json2scss-map-webpack-importer');

module.exports = {
  module: {
    rules: [
      test: /\.s?[ac]ss$/i,
      use: [
        'css-loader',
        {
          loader: 'sass-loader',
          options: {
            importer: jsonImporter(),
          },
        },
      ],
    ],
  },
};
```

```scss
@import 'vars.json';

.my-div {
  color: map-get($vars, 'foreground');
}
```

## Options

All options are documented on the [json2scss-map
documentation](https://github.com/AS-Devs/json2scss-map#json2scssopts).

If the prefix option is not specified, the map will be set as a
variable with the same name as the file imported. For example, given a
json file `test.json`

```json
{
    "bgColor": red
}
```

And a sass file which imports it:

```scss
@import 'test.json';
```

The map variable will be named `$test` and can be accessed with
`map-get`

```scss
.my-class {
  background: map-get($test, 'bgColor');
}
```
