const path = require('path');
const { Readable } = require('stream');
const json2scss = require('json2scss-map');

module.exports =
  (options = {}) =>
  (url, context, done) => {
    if (!(url.endsWith('.json') || url.endsWith('.js'))) return done(null);

    const filename = path.resolve(path.dirname(context), url);
    const data = JSON.stringify(require(filename));

    options.prefix = options.prefix || `$${path.parse(url).name}:`;

    const stream = Readable.from([data]).pipe(json2scss(options));

    const chunks = [];
    stream.on('data', (chunk) => chunks.push(Buffer.from(chunk)));
    stream.on('error', (err) => {
      throw err;
    });
    stream.on('end', () => {
      done({
        contents: Buffer.concat(chunks).toString(),
        syntax: 'scss',
      });
    });
  };
